# 0x666f6f Public

### What is this?
This is a public repository to host files associated with the blog and projects on this account.

### Blog? Check out [0x666f6f.bitbucket.io](https://0x666f6f.bitbucket.io).

### Files? Check out [https://bitbucket.org/0x666f6f/0x666f6f-public/src](https://bitbucket.org/0x666f6f/0x666f6f-public/src).

### Usage policy? This project is licensed under the terms of the [MIT license](https://bitbucket.org/0x666f6f/0x666f6f-public/src/master/LICENSE.md).